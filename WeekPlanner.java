

import java.util.Scanner;

public class WeekPlanner {
    public static void main(String[] args) {


        String [] [] schedule= new String[7][2];

        //sunday
        schedule [0][0]="Sunday";
        schedule [0][1]="do home work";


        
        //monday
        schedule [1][0]="Monday";
        schedule[1][1]="go to courses; watch a movie";

        
        //tuesday
        schedule [2][0]="Tuesday";
        schedule[2][1]="finish the project";

        //wednesday
        schedule [3][0]="Wednesday";
        schedule[3][1]="Start a new project";



        //thursday
        schedule [4][0]="Thursday";
        schedule[4][1]="Push your projects to Github";

        //friday
        schedule [5][0]="Friday";
        schedule[5][1]="Read some documentation";


        //saturday
        schedule [6][0]="Saturday";
        schedule[6][1]="Go for a walk";


        //starting
        System.out.println("Welcome to my program \n ");
        
        Scanner d=new Scanner(System.in);

        boolean correctness=false; //I created this for controlling the program

        while(correctness==false) {
            System.out.print("Please, input the day of the week: ");
            String day=d.nextLine();
            switch (day) {
                case "Sunday" : case "SUNDAY" : case "sunday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[0][1]+"\n" );
                    break;

                case "Monday" : case "monday" : case "MONDAY":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[1][1]+"\n");
                    break;

                case "Tuesday" : case "TUESDAY" : case "tuesday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[2][1]+"\n");
                    break;

                case "Wednesday" : case "WEDNESDAY" : case "wednesday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[3][1]+"\n");
                    break;

                case "Thursday" : case "THURSDAY" : case "thursday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[4][1]+"\n");
                    break;

                case "Friday" : case "FRIDAY" : case "friday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[5][1]+"\n");
                    break;

                case "Saturday" : case "SATURDAY" : case "saturday":
                    System.out.println("Your tasks for " + day + ":" + " " + schedule[6][1]+"\n");
                    break;

                case "exit" : case "EXIT": case "Exit":
                    correctness=true;
                    break;

                default: // Optional
                    System.out.println("Sorry, I don't understand you, please try again.");
                    correctness=false;
                    break;
            }
        }
        System.out.println("Program ends.Thanks for using my program");









    }
}
